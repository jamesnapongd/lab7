package distanceConverter;

import javax.swing.*;

import org.omg.Messaging.SyncScopeHelper;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * converter GUI 
 * @author Napong Dungduangsasitorn
 *
 */
public class ConverterUI extends JFrame
{

	private JTextField txtConvertFrom  = new JTextField(10);
	private JTextField txtConvertTo = new JTextField(10);
	private JComboBox boxConvertFrom = new JComboBox();
	private JComboBox boxConvertTo = new JComboBox();

	/**
	 * Constructor convertorUI.
	 */
	public ConverterUI() {
		this.setTitle("Length Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.initComponents();
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {

		UnitConverter uc = new UnitConverter();
		Unit[] units = uc.getUnits();
		JFrame frame = new JFrame();
		JPanel pane = new JPanel();
		pane.setLayout(new FlowLayout());
		JLabel label = new JLabel(" = ");
		txtConvertTo.setEditable(false);
		for(int i = 0;i < units.length;i++){
			boxConvertFrom.addItem(units[i]);
			boxConvertTo.addItem(units[i]);
		}
		
		JButton convertButton = new JButton("Convert");
		JButton clearButton = new JButton("Clear");

		
		convertButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					if(Double.parseDouble(txtConvertFrom.getText())<0){
						JOptionPane.showMessageDialog(null,"Cant Compute Negative Number","Error",JOptionPane.WARNING_MESSAGE);
					}
					else{
						txtConvertTo.setText(String.format("%.6f",uc.convert(Double.parseDouble(txtConvertFrom.getText()), 
								(Unit)boxConvertFrom.getSelectedItem(), (Unit)boxConvertTo.getSelectedItem())));
					}
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null,"Please Insert Number","Error",JOptionPane.WARNING_MESSAGE);
				}
			}
		});

		txtConvertFrom.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e){
						try{
							if(Double.parseDouble(txtConvertFrom.getText())<0){
								JOptionPane.showMessageDialog(null,"Cant Compute Negative Number","Error",JOptionPane.WARNING_MESSAGE);
							}
							else{
								txtConvertTo.setText(String.format("%.6f",uc.convert(Double.parseDouble(txtConvertFrom.getText()), 
										(Unit)boxConvertFrom.getSelectedItem(), (Unit)boxConvertTo.getSelectedItem())));
							}
						} catch (NumberFormatException ex) {
							JOptionPane.showMessageDialog(null,"Please Insert Number","Error",JOptionPane.WARNING_MESSAGE);
						}

						       
					}
				});

		clearButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				txtConvertFrom.setText("");
				txtConvertTo.setText("");
			}
		});

		

		pane.add(txtConvertFrom);
		pane.add(boxConvertFrom);

		pane.add(label);
		pane.add(txtConvertTo);
		pane.add(boxConvertTo);
		pane.add(boxConvertTo);
		pane.add(convertButton);
		pane.add(clearButton);
		frame.add(pane);
		frame.pack();
		frame.setVisible(true);
	}
}






