package distanceConverter;


public class UnitConverter {

	/**
	 * convert any unit to any unit.
	 * @param amount value that want to convert.
	 * @param fromUnit unit that want to convert to another unit.
	 * @param toUnit unit that want to convert.
	 * @return convert is value that convert from fromUnit to toUnit.
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit){
		double convert = amount * fromUnit.getValue() / toUnit.getValue();
		return convert;
	}

	/**
	 * get unit from length class.
	 * @return units is array of unit from length class.
	 */
	public Unit[] getUnits(){
		Unit[] units =  {Length.CENTIMETER,Length.FOOT
				,Length.KILOMETER,Length.METER,Length.MILE
				,Length.WA,Length.LIGHT_YEAR,Length.MICRON};

		return 	units;
	}

}
